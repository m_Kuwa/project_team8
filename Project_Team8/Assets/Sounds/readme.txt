﻿もし新規に入れたい場合は、入れたいオブジェクトにAudioSourceコンポーネントを追加。
音を鳴らす処理を組み込みたいスクリプトの最初にAudioSource audio;引数の追加
AudioClip 引数名（お好みで）を追加。
Start()内にaudio=GetComponent<AudioSource>();を追加。

その後処理したいメソッドの中に
audio.clip=指定した引数名;
audio.Play()を割り込ませる。
その後、ヒエラルキー内で引数名のところに好きな音源を追加すればSEがなります。

注意点
↑の処理は「直接」Update()内に書き込まないように！！大変なことになります。

