﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKill : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController cl = other.GetComponent<PlayerController>();
            cl.Miss();
        }
    }
}
