﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

/// <summary>
/// シーン名の列挙型
/// </summary>
public enum SceneName
{
    Title,
    Credit,
    StageSelect,
    Stage01,
    Stage02,
    Stage03,
    GameOver,
    Clear,
    GoodEnd,
    Null,
}

/// <summary>
/// 各種シーンの管理者
/// 作成者:谷 永吾
/// </summary>
public class SceneManagement : MonoBehaviour
{
    private SceneName playerStage;

    // Use this for initialization
    void Start()
    {
        //シーンが遷移してもこのスクリプトのアタッチされたオブジェクトは破壊されない
        DontDestroyOnLoad(this);

        CurrentScene = SceneName.Null;
        playerStage = SceneName.Null;
    }

    // Update is called once per frame
    void Update()
    {
        switch (CurrentScene)
        {
            case SceneName.Title:
                TitleUpdate();
                break;

            case SceneName.Credit:
                CreditUpdate();
                break;

            case SceneName.StageSelect:
                SelectUpdate();
                break;

            case SceneName.Stage01:
                Stage1Update();
                break;

            case SceneName.Stage02:
                Stage2Update();
                break;

            case SceneName.Stage03:
                Stage3Update();
                break;

            case SceneName.GameOver:
                GameOverUpdate();
                break;

            case SceneName.Clear:
                ClearUpdate();
                break;
            case SceneName.GoodEnd:
                GoodEndUpdate();
                break;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void Stage1Update()
    {
        if (CurrentScene != SceneName.Stage01)
            return;

        playerStage = SceneName.Stage01;
    }

    private void Stage3Update()
    {
        if (CurrentScene != SceneName.Stage03)
            return;

        playerStage = SceneName.Stage03;
    }

    private void Stage2Update()
    {
        if(CurrentScene != SceneName.Stage02)
        {
            return;
        }

        playerStage = SceneName.Stage02;
    }

    public static void Load(SceneName name)
    {
        Debug.Log(name);

        CurrentScene = name;
        SceneManager.LoadScene(name.ToString());
    }

    private void TitleUpdate()
    {
        if (CurrentScene != SceneName.Title)
            return;
    }

    private void SelectUpdate()
    {
        if (CurrentScene != SceneName.StageSelect)
            return;

        //今後はステージ番号を選べるようにする
        if (Input.GetButtonDown("Button_A") || Input.GetKeyDown(KeyCode.Alpha4))
            Load(SceneName.Stage01);
    }

    private void GameOverUpdate()
    {
        if (CurrentScene != SceneName.GameOver)
            return;

        if (Input.GetButtonDown("Button_A"))
            Load(SceneName.Title);
    }

    private void CreditUpdate()
    {
        if (CurrentScene != SceneName.Credit)
            return;

        if (Input.GetKeyDown(KeyCode.Alpha5))
            Load(SceneName.Title);
    }

    private void ClearUpdate()
    {
        if (CurrentScene != SceneName.Clear)
            return;
        
        switch (playerStage)
        {
            case SceneName.Stage01:
            case SceneName.Stage02:
                LoadNextStage();
                break;

            //後ほどここをTrueEndなどにしたい所
            case SceneName.Stage03:
                Load(SceneName.GoodEnd);
                break;
        }
    }
    private void GoodEndUpdate()
    {
        if (CurrentScene != SceneName.GoodEnd)
            return;

        if (Input.GetButtonDown("Button_A"))
            Load(SceneName.Title);
    }

    public static SceneName CurrentScene { get; private set; }

    private void LoadNextStage()
    {        
        int nextStageNum = (int)playerStage + 1;
        Load((SceneName)nextStageNum);
    }
}
