﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title_EventManager : MonoBehaviour
{
    [SerializeField, Header("タイトル画像")]
    SpriteRenderer title;
    [SerializeField, Header("クレジット画像")]
    SpriteRenderer credit;
    [SerializeField, Header("ストーリー描写絵")]
    SpriteRenderer story;
    [SerializeField, Header("ストーリーの文字")]
    GameObject moji;
    [SerializeField, Header("黒いの")]
    SpriteRenderer fade;
    [SerializeField, Header("Press Anykey")]
    SpriteRenderer press;

    bool creditEnd;

    enum state
    {
        idle,
        credit,
        fadeIn,
        isStory,
        finish,
        fadeOut,
        skip,
    }
    state currentState;

    // Start is called before the first frame update
    void Start()
    {
        story.color -= new Color(0, 0, 0, 1);
        currentState = state.idle;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (currentState)
        {
            case state.idle:
                Idle();
                break;

            case state.credit:
                Credit();
                break;

            case state.fadeIn:
                FadeIn();
                break;

            case state.isStory:
                IsStory();
                break;

            case state.finish:
                Finish();
                break;

            case state.fadeOut:
                FadeOut();
                break;

            case state.skip:
                Skip();
                break;
        }
    }

    void Update()
    {
        if (currentState == state.credit)
            return;
        if (!Input.GetButtonDown("Button_A")) 
            return;

        currentState = state.skip;
    }

    void Idle()
    {
        if (Input.GetButton("Back") || Input.GetKeyDown(KeyCode.Alpha0))
        {
            creditEnd = false;
            currentState = state.credit;
            return;
        }

        if (Input.anyKeyDown)
        {
            currentState = state.fadeIn;
            press.GetComponent<AutoFadeInOut>().Stop();
        }
    }

    void Credit()
    {
        if (Input.anyKeyDown)
        {
            creditEnd = true;
        }
        if (!creditEnd)
        {
            if (credit.color.a >= 1)
                return;
            credit.color += new Color(0, 0, 0, 0.1f);
            return;
        }
        credit.color -= new Color(0, 0, 0, 0.1f);
        if (credit.color.a <= 0)
            currentState = state.idle;

    }

    void FadeIn()
    {
        if (story.color.a < 1)
            story.color += new Color(0, 0, 0, 0.01f);
        if (story.color.a >= 1.0f)
        {
            currentState = state.isStory;
        }
    }

    void IsStory()
    {
        if (story.color.b >= 0.5f)
            story.color -= new Color(0.005f, 0.005f, 0.005f, 0);
        else if (moji.transform.position.y <= 3.5f)
            moji.transform.position += new Vector3(0, 0.03f, 0);
        else
        {
            moji.GetComponent<AutoFadeInOut>().enabled = true;
            currentState = state.finish;
        }
    }

    void Finish()
    {
        if (Input.anyKey)
        {
            currentState = state.fadeOut;
        }
    }

    void FadeOut()
    {
        story.color -= new Color(0.01f, 0.01f, 0.01f, 0);
        moji.GetComponent<SpriteRenderer>().color -= new Color(0, 0, 0, 0.1f);

        if (story.color.b <= 0.01f)
            SceneManagement.Load(SceneName.Stage01);
    }

    void Skip()
    {
        fade.color += new Color(0, 0, 0, 0.01f);
        if (fade.color.a < 1)
            return;

        SceneManagement.Load(SceneName.Stage01);
    }
}
