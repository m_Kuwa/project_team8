﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRespawn : MonoBehaviour
{
    public Transform respawn;
    Collider hit;
    bool check;     //通過済みか
    [SerializeField, Header("旗オブジェクト")]
    GameObject Flag;
    FlagController fControl;
    
    // Start is called before the first frame update
    void Start()
    {
        hit = null;
        check = false;

        Flag.SetActive(true);

        //所有する旗のアニメーションは別スクリプトに委託
        fControl = Flag.GetComponent<FlagController>();
        fControl.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider hit)
    {
        if (check)
            return;

        this.hit = hit;
        if (hit.tag == "Player")
        {
            RespawnSet();
            check = true;
        }
    }
    void RespawnSet()
    {
        PlayerController cl = hit.GetComponent<PlayerController>();
        cl.RespawnSet(respawn.position);
        cl.PlanetChange(gameObject);

        //旗を起立
        fControl.SetStandAnim();
    }
}
