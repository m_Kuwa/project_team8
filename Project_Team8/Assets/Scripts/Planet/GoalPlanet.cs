﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalPlanet : MonoBehaviour
{
    [SerializeField]

    public GameObject missFader;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(missFader.GetComponent<ClearFader>().IsClear())
            SceneManagement.Load(SceneName.Clear);
    }

    private void OnTriggerEnter(Collider hit)
    {
        if(hit.tag=="Player")
        {
            missFader.GetComponent<ClearFader>().PaintOn();
        }
    }
}
