﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRevolution : MonoBehaviour
{
    // Start is called before the first frame update
    bool isEnd;
    private float timer=60f;
    public Transform RevolutionPoint;
    public float speed = 5f;
    void Start()
    {
        isEnd = false;
        timer = 60f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void FixedUpdate()
    {
        if(!isEnd)
        DynamicRevolution();

        //if (Input.GetKeyDown(KeyCode.Space))
        //    Destruction();
        //if (isEnd)
        //    TimeCount();
    }
    void DynamicRevolution()
    {
        transform.RotateAround(RevolutionPoint.position, new Vector3(0,0,1), speed);
    }
    void IsEndOn()
    {
        isEnd = true;
    }
    void IsEndOff()
    {
        isEnd = false;
    }
    void Destruction()
    {
        IsEndOn();

    }
    void TimeCount()
    {
        timer = timer - 1;
        if(timer==0)
        {
            Tereport();
            IsEndOff();
            timer = 60f;
        }
    }
    void Tereport()
    {
        transform.position = new Vector3(5, 0, 0);
    }
}
