﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 惑星ごとのPlayer公転時のズーム倍率指定処理クラス
/// 作成者:谷 永吾
/// </summary>
public class OrderZoomValue : MonoBehaviour
{
    AudioSource audio;
    public AudioClip RotateOn;
    [SerializeField, Header("Playerが公転状態になった時のズーム割合(％)")]
    int zoomValue = 100;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public int GetZoomValue()
    {
        return zoomValue / 100;
    }
    private void OnTriggerEnter(Collider hit)
    {
        audio.clip = RotateOn;
        audio.Play();
    }
}
