﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class rLives : MonoBehaviour
{
    [SerializeField, Header("プレイヤー")]
    GameObject player;
    private PlayerController pCl;
    private Text text;

    // Start is called before the first frame update
    void Start()
    {
        pCl = player.GetComponent<PlayerController>();
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = pCl.RLive().ToString();
    }
}
