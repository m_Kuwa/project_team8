﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    [SerializeField]
    GameObject parent;

    PlayerController cl;
    Vector3 dir;
    // Start is called before the first frame update
    void Start()
    {
        cl = parent.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        dir = cl.GetMovedir();
        //transform.rotation = Quaternion.LookRotation(new Vector3(dir.y, -dir.x, 10), Vector3.back);
        transform.rotation = Quaternion.FromToRotation(Vector3.right, dir);
    }
}
