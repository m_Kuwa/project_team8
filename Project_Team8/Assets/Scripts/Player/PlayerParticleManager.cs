﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Playerのパーティクルのマネージャー
/// </summary>
public class PlayerParticleManager : MonoBehaviour
{
    private bool play;
    [SerializeField, Header("公転離脱時のパーティクル")] ParticleSystem breakawayParticle;
    
    private Vector3 direction;

    private void Start()
    {
        play = false;

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //パーティクル再生中でなければ
        if (!play)
            //帰る
            return;

        //パーティクルの位置を後ろに
        breakawayParticle.transform.position -= (direction * 0.02f);
    }

    /// <summary>
    /// 公転離脱時エフェクト開始
    /// </summary>
    /// <param name="direction">移動方向</param>
    public void BreakawayParticleStart(Vector3 direction)
    {
        //こっちのdirectionにparticleの生成される円が移動方向と逆の方向を向く方向を入れてるはず（よくわかってない）
        breakawayParticle.transform.position = transform.position;
        this.direction = direction.normalized;
        breakawayParticle.transform.rotation = Quaternion.LookRotation(new Vector3(direction.y, -direction.x, 0), Vector3.forward);
        breakawayParticle.Play();
        play = true;
    }

    public void ParticleEnd()
    {
        breakawayParticle.Stop();
    }
}
