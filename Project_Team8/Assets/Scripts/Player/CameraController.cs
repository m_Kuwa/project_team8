﻿using UnityEngine;
using static PlayerController;

/// <summary>
/// フォーカス対象
/// </summary>
enum FocusTarget
{
    Player,
    ToPlayer,
    ToPlanet,
    Planet,
}

/// <summary>
/// メインカメラを制御するクラス
/// 作成者:谷 永吾
/// </summary>
public class CameraController : MonoBehaviour
{
    int timescale = 2;
    //フィールド
    private Vector3 offset; //Playerと並走する際の座標補正値

    [SerializeField, Header("ゲーム開始時フォーカス基準となるプレイヤー")]
    Transform player;
    [SerializeField, Header("カメラワークの速度")]
    float speed = 5f;

    [SerializeField, Header("カメラ操作時の移動速度・範囲")]
    float inputMoveSmoothness = 5f;

    [SerializeField, Header("カメラの最大可動座標(外枠の死亡ブロックとカメラの視野に合わせて調整してください。)")]
    Vector2 maxCordinate = new Vector2(50, 50);
    [SerializeField, Header("カメラの最小可動座標(外枠の死亡ブロックとカメラの視野に合わせて調整してください。)")]
    Vector2 minCordinate = new Vector2(-50, -50);

    private PlayerController pControl;
    private playerStatus playerStatus;
    private Mediator mediator;

    //現在フレームと1フレーム前のフォーカス対象
    private FocusTarget mode, preMode;
    //現在フレームと１フレーム前のフォーカス対象のオブジェクト
    private Transform target, preTarget;

    //Playerが惑星公転中のカメラズーム倍率
    [SerializeField, Header("惑星公転中の視野範囲(大きいほどオブジェクトは小さくなります)")]
    float revoFocusSize = 6f;
    [SerializeField, Header("公転開始・終了時の視野範囲の変化速度")]
    float sizeChangeSpeed = 0.5f;
    private float initFocusSize;

    private float gameStartTime;

    // Start is called before the first frame update
    void Start()
    {
        timescale = 2;
        //プレイヤーとの補正距離を計測
        offset = this.transform.position - player.position;

        pControl = player.GetComponent<PlayerController>();

        GameObject mediatorObj = GameObject.Find("GameMediator");
        mediator = mediatorObj.GetComponent<Mediator>();

        mode = FocusTarget.Player;
        preMode = mode;
        playerStatus = pControl.GetStatus();

        target = player;
        preTarget = null;

        initFocusSize = Camera.main.orthographicSize;
        gameStartTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        switch (playerStatus)
        {
            case playerStatus.free:
            case playerStatus.freeze:
                if (preMode == FocusTarget.Planet || preMode == FocusTarget.ToPlanet)
                    mode = FocusTarget.ToPlayer;

                if (mode == FocusTarget.ToPlayer)
                {
                    //Playerを線形的に追いかける処理
                    LerpFocus_ToPlayer();
                }

                if (mode == FocusTarget.Player)
                {
                    //Playerに追随するカメラワーク
                    FocusOnPlayer();
                }

                break;

            case playerStatus.rotate:
            case playerStatus.blackHole:
                if (mode == FocusTarget.Planet &&
                    target == preTarget)
                {
                    FocusOnPlanet();
                    InputMoveXY();
                }
                else
                {
                    //フォーカス対象を惑星へ切り替え
                    mode = FocusTarget.ToPlanet;
                    //線形的にカメラの視野角を広くする(物を小さく映す)
                    LerpFocus_ToPlanet();
                }
                break;
        }

        //Debug.Log(this.transform.position.ToString());
        //各種データのの更新
        UpdateFocusData();
    }

    /// <summary>
    /// フレームごとのデータの更新
    /// </summary>
    private void UpdateFocusData()
    {
        preMode = mode;//フォーカスモード
        preTarget = target;//フォーカス対象
        playerStatus = pControl.GetStatus();//Playerの状態
    }

    /// <summary>
    /// カメラをキー操作で移動させる
    /// </summary>
    private void InputMoveXY()
    {
        //キー入力に応じた移動量を生成
        Vector2 movement = Vector2.zero;
            float movX = Input.GetAxis("Horizontal");
            float movY = Input.GetAxis("Vertical");
            Vector2 addValue = new Vector2(movX, movY) * inputMoveSmoothness;
            movement += addValue;

        //可動範囲内で指定された範囲だけ移動
        Vector2 movedPos = ClampCordinate(
            new Vector2(
                this.transform.position.x + movement.x, 
                this.transform.position.y + movement.y));
        this.transform.position =
            new Vector3(movedPos.x, movedPos.y, this.transform.position.z);
    }

    private void FocusOnPlayer()
    {
        target = player;
        CloseFocus();

        this.transform.position = target.position + offset;

        //座標を可動範囲内に補正
        Vector2 fixedPos = ClampCordinate(
            new Vector2(this.transform.position.x, this.transform.position.y));
        this.transform.position = new Vector3(fixedPos.x, fixedPos.y, this.transform.position.z);
    }

    private void FocusOnPlanet()
    {
        BackFocus();

        //可動領域内に収めつつ惑星を中心に据える
        Vector2 fixedPos = ClampCordinate(CalcCamPos_Planet());
        this.transform.position = new Vector3(fixedPos.x, fixedPos.y, this.transform.position.z);
    }

    /// <summary>
    /// 惑星を中心に映すカメラ位置の取得
    /// </summary>
    /// <returns>カメラ位置</returns>
    private Vector2 CalcCamPos_Planet()
    {
        var fixedPos = pControl.GetParent().transform.position;
        return fixedPos;
    }

    private void LerpFocus_ToPlanet()
    {
        switch (mode)
        {
            case FocusTarget.ToPlanet:
                if (preMode == FocusTarget.Player || preMode == FocusTarget.ToPlayer)
                {
                    gameStartTime = SetLerpTime();
                }
                break;
            default:
                return;
        }

        target = pControl.GetParent().transform;
        mode = FocusTarget.ToPlanet;

        BackFocus();//徐々にカメラを引いていく

        Vector3 endPos = new Vector3(target.position.x, target.position.y, this.transform.position.z);

        //可動領域の外の場合は補正を行う(この時点での補正はカメラワークのバグの原因のため削除)
        //endPos.x = Mathf.Clamp(endPos.x, minCordinate.x, maxCordinate.x);
        //endPos.y = Mathf.Clamp(endPos.y, minCordinate.y, maxCordinate.x);
        //Debug.Log(endPos.ToString());

        //線形移動中も可動領域に行こうとしたら補正する
        this.transform.position = Vector3.Lerp(this.transform.position, endPos, Time.deltaTime * speed);
        Vector2 currentPos = ClampCordinate(new Vector2(this.transform.position.x, this.transform.position.y));
        this.transform.position = new Vector3(currentPos.x, currentPos.y, this.transform.position.z);

        //惑星をある程度中心に据えたらモード切替
        //Vector2 camPosVec2 = new Vector2(
        //    Mathf.Clamp(this.transform.position.x, minCordinate.x, maxCordinate.x),
        //    Mathf.Clamp(this.transform.position.y, minCordinate.y, maxCordinate.y));

        Vector2 endPosVec2 = ClampCordinate(new Vector2(endPos.x, endPos.y));
        float distance = Vector2.Distance(currentPos, endPosVec2);

        if (distance <= 0.1f)
        {
            mode = FocusTarget.Planet;
        }
    }

    private void LerpFocus_ToPlayer()
    {
        switch (mode)
        {
            case FocusTarget.ToPlayer:
                if (preMode == FocusTarget.Planet || preMode == FocusTarget.ToPlanet)
                {
                    gameStartTime = SetLerpTime();
                }
                break;
            default:
                return;
        }

        target = player;

        CloseFocus();//徐々にカメラを近づけていく

        Vector3 endPos = player.position + offset;
        Vector2 clamped = ClampCordinate(new Vector2(endPos.x, endPos.y));
        endPos = new Vector3(clamped.x, clamped.y, endPos.z);

        Vector2 offsetVec2 = new Vector2(offset.x, offset.y);
        Vector2 CamPosVec2 = new Vector2(this.transform.position.x, this.transform.position.y);
        float distance = Vector2.Distance(CamPosVec2, offsetVec2);
        float rate = (Time.time - gameStartTime) * speed / distance;

        #region Debug処理
        //Debug.Log(endPos.ToString());
        //Debug.Log(rate.ToString());
        //Debug.Log(Time.time.ToString());
        //Debug.Log(gameStartTime.ToString());
        #endregion Debug処理

        this.transform.position = Vector3.Lerp(this.transform.position, endPos, rate);
        Vector2 clampedCamPos = ClampCordinate(new Vector2(this.transform.position.x, this.transform.position.y) );
        this.transform.position = new Vector3(
            clampedCamPos.x,clampedCamPos.y, this.transform.position.z);
        
        //終点まで移動できたらモード切替
        if (rate >= 0.9f || distance <= 3)
        {
            mode = FocusTarget.Player;
        }
    }

    private float SetLerpTime()
    {
        float start = Time.time;
        return start;
    }

    /// <summary>
    /// カメラを引きで取る（カメラの視野範囲を拡張）
    /// </summary>
    private void BackFocus()
    {
        if (Camera.main.orthographicSize >= revoFocusSize * mediator.ZoomValue)
        {
            Camera.main.orthographicSize = revoFocusSize;
            return;
        }

        Camera.main.orthographicSize += sizeChangeSpeed;
    }

    /// <summary>
    /// カメラを寄りで撮る（カメラの視野範囲の縮小）
    /// </summary>
    private void CloseFocus()
    {
        if (Camera.main.orthographicSize <= initFocusSize)
        {
            Camera.main.orthographicSize = initFocusSize;
            return;
        }

        Camera.main.orthographicSize -= sizeChangeSpeed;
    }

    /// <summary>
    /// 補正後の座標を演算・取得
    /// </summary>
    /// <param name="cordinate">補正前の座標値(2次元)</param>
    /// <returns>補正後の座標値(2次元)</returns>
    private Vector2 ClampCordinate(Vector2 cordinate)
    {
        return new Vector2(
            Mathf.Clamp(cordinate.x, minCordinate.x, maxCordinate.x),
            Mathf.Clamp(cordinate.y, minCordinate.y, maxCordinate.y));
    }
}
