﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoFadeInOut : MonoBehaviour
{
    Color addColor = new Color(0, 0, 0, 0.01f);
    SpriteRenderer spr;

    enum fade
    {
        fadeIn,
        fadeOut,
        end,
    }
    fade f;
    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        switch(f)
        {
            case fade.fadeIn:
                FadeIn();
                break;

            case fade.fadeOut:
                FadeOut();
                break;

            case fade.end:
                break;
        }
    }

    void FadeIn()
    {
        spr.color += addColor;
        if (spr.color.a >= 1)
            f = fade.fadeOut;
    }

    void FadeOut()
    {
        spr.color -= addColor;
        if (spr.color.a <= 0)
            f = fade.fadeIn;
    }

    public void Stop()
    {
        spr.color += new Color(0, 0, 0, 1);
        f = fade.end;
    }
}
